resource "aws_iam_role" "app_role" {
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "app_instance_profile" {
  name_prefix = "webapp-instance-profile"
  role = aws_iam_role.app_role.id
}

data "aws_elb_service_account" "elb_service_account" {}

resource "aws_s3_bucket_policy" "alb_log_policy" {
  bucket = aws_s3_bucket.s3_logs_bucket.id

  policy = <<EOF
{
    "Id": "Policy",
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": [
          "s3:PutObject"
        ],
        "Effect": "Allow",
        "Resource": "${aws_s3_bucket.s3_logs_bucket.arn}/logs/*",
        "Principal": {
          "AWS": ["${data.aws_elb_service_account.elb_service_account.arn}"]
        }
      }
    ]
  }
EOF
}