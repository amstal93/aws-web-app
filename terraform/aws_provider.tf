provider "aws" {
  region = "eu-central-1"
  version = "~> 2.44"
}

provider "template" {
  version = "~> 2.1"
}